#!/bin/bash
export DB_DIRECTORY=database  # relative path to the directory where the db files are stored 
export DAEMON_URL=http://yourusername:yourpassword@localhost:19119/
export COIN=ECCoin
export HOST=
export SSL_CERTFILE=$(pwd)/server.crt
export SSL_KEYFILE=$(pwd)/server.key
export SERVICES=ssl://:50002,rpc://:8000
export DONATION_ADDRESS="EZ5Lj6dWPouwsMc9je2Vh1CwubQGauafa7" # visible in the client
./electrumx_compact_history
